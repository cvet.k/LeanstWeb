/*global $ ,jQuery ,alert, console, Typed, google */

// Loading Screen 
    
$(window).on('load', function () {
    
    "use strict";

    $('.loading-overlay .sk-cube-grid').fadeOut(1000, function () {

        $(this).parent().fadeOut(1000, function () {

            $('body').css('overflow', 'auto');
//            $(this).remove();

        });

    });

});

$(document).ready(function () {
   
    "use strict";   
    
    // Horizontal Navbar
    
    $('.nav-toggle').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.navbar').toggleClass('open');
        $('.navbar .nav-item').toggleClass('show-li-nav');
        $('.nav-toggle').toggleClass('toggle');
    });
    
    $('.navbar-toggler').on('click', function (e) {
        
        $('.navbar-toggler .menu').toggleClass('toggle');
        
    });
    
    $('.navbar-toggler').on('click', function () {
        
        $('.navbar').toggleClass('open-nav');
        
    });
        
    $('html').on('keydown', function (e) {
        
        if (e.keyCode === 27) { // ESC
             
            if ($('.navbar').hasClass('open')) {
                $('.navbar').removeClass('open');
                $('.navbar .nav-item').removeClass('show-li-nav');
                $('.nav-toggle').removeClass('toggle');
            }             
        }        
    });
    
    // Vertical Navbar 
    
    $('.navbar .collapse-menu').on('click', 'span', function () {
        $('.navbar, .name-menu, .navbar-brand img, .collapse-name, .header .title, .nav-item i, .header .down a i, section, footer').toggleClass('close-menu');
        $('.collapse-icon').toggleClass('closed-menu').toggleClass('open-menu');
    });
    
});
